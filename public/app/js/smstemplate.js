// var emailContentText = "Hi ,\n\nHope you're well. I'm writing to know how our business can help you.\n\nPlease choose a event to schedule an appointment:\n\n${eventsUrls} See you soon!\n\n %ownerName% \n\n %companyName%";
		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordId;
		var recordModule;
		var ButtonPosition = 

        document.addEventListener("DOMContentLoaded", async function(event) {
        	// var cusotmerData = ["Owner", "Email", "$currency_symbol", "Other_Phone", "Mailing_State", "$upcoming_activity", "Other_State", "Other_Country", "Last_Activity_Time", "Department", "$process_flow", "Assistant", "Mailing_Country", "id", "$approved", "Reporting_To", "$approval", "Other_City", "Created_Time", "$editable", "Home_Phone", "$status", "Created_By", "Secondary_Email", "Description", "Vendor_Name", "Mailing_Zip", "$photo_id", "Twitter", "Other_Zip", "Mailing_Street", "Salutation", "First_Name", "Full_Name", "Asst_Phone", "Record_Image", "Modified_By", "Skype_ID", "Phone", "Account_Name", "Email_Opt_Out", "Modified_Time", "Date_of_Birth", "Mailing_City", "Title", "Other_Street", "Mobile", "Last_Name", "Lead_Source", "Tag", "Fax"];
			ZOHO.embeddedApp.on("PageLoad", async function(record) {
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(value){
				  	userFields = value.fields;
					value.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});
				var templateModules="";
				var historyFields = await ZOHO.CRM.META.getFields({"Entity":"sainosmsforzohocrm__Saino_SMS_History"});
				historyFields.fields.forEach(function(field){
					if(field.data_type == "lookup" ){
						templateModules = templateModules +'<li class="templateItem"  onclick="selectModule('+"'"+field.lookup.module.api_name+"'"+')">'+field.lookup.module.api_name+'</li>';
					}
				});	
				$('#templateList').append(templateModules);
				if(record.EntityId){
	               	recordId = record.EntityId[0];
	               	recordModule = record.Entity;
	               	ButtonPosition = record.ButtonPosition;
	               	if(ButtonPosition == "DetailView" || ButtonPosition == "ListViewEachRecord"){
		               	ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordId})
						.then(function(data){
							document.getElementById("templateName").value = data.data[0].Name;
							selectModule(data.data[0].sainosmsforzohocrm__Module_Name);
							document.getElementById("emailContentEmail").innerText = data.data[0].sainosmsforzohocrm__Message;
						})
					}
				}	
				else{
					selectModule("Contacts");
				}	
	        });

	        ZOHO.embeddedApp.init();
	        const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 1600;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}

        });
        	
		function selectModule(module){
			document.getElementById("selectedmodule").innerText = module;
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var smsContent = document.getElementById("emailContentEmail").innerText;
			if(module == "Leads"){
				if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Events.") != -1 || smsContent.indexOf("${Deals.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
			}
			else if(module == "Contacts"){
				if(smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Events.") != -1 || smsContent.indexOf("${Deals.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
			}
			else if(module == "Deals"){
				if(smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Events.") != -1 || smsContent.indexOf("${Contacts.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
			}
			else if(module == "Events"){
				if(smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Deals.") != -1){
					document.getElementById("emailContentEmail").innerHTML ="";
				}
				customerData = ["Receiver First Name","Receiver Last Name","Receiver Full Name"];
			}
			document.getElementById("dropdown-menu-email").innerHTML="";
			ZOHO.CRM.META.getFields({"Entity":module}).then(function(data){
				customerData.forEach(function(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
				});
				data.fields.forEach(function(field){
					addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
				});
			});	
		}
        function showEmail(editor){
			for(var i=0; i<emailContent.length;i++){
				if(emailContent[i].emailId == editor.id){
					document.getElementById("emailContentEmail").innerHTML = emailContent[i].emailContent;
					document.getElementById("subjectEmail").innerHTML=emailContent[i].subject;
					break;
				}
			}
		}
        function saveTemplate(){
        	var name = document.getElementById("templateName").value;
        	var templateModule = document.getElementById("selectedmodule").innerText;
        	var smsContent = document.getElementById("emailContentEmail").innerText;
        	var othermodule=false;
        	if(templateModule == "Leads" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Events.") != -1)){
				othermodule=true;
			}
			else if(templateModule == "Contacts" && (smsContent.indexOf("${Leads.") != -1 || smsContent.indexOf("${Events.") != -1)){
				othermodule=true;
			}
			else if(templateModule == "Events" && (smsContent.indexOf("${Contacts.") != -1 || smsContent.indexOf("${Leads.") != -1)){
				othermodule=true;
			}
			if(othermodule){
				document.getElementById("ErrorText").innerText = "Message Contains Other Modules Merge Fields.Please change it.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 2000);
				return ;
			}
        	if(name == ""){
        		document.getElementById("ErrorText").innerText = "Template Name cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return ;
        	}
        	if(templateModule == ""){
        		document.getElementById("ErrorText").innerText = "Please Choose Module";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
				return ;
        	}
        	if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"") == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
	        	document.getElementById("ErrorText").innerText = "SMS Template is saving...";
				document.getElementById("Error").style.display= "block";
	        	var func_name = "sainofirstsmsforzohocrm__smshandler";
	        	var message = document.getElementById("emailContentEmail").innerText;
				var req_data = {"Name":name,"sainosmsforzohocrm__Message":message,"sainosmsforzohocrm__Module_Name":templateModule}
	        	if(ButtonPosition){
	        		req_data["id"]=recordId;
					ZOHO.CRM.API.updateRecord({Entity:"sainosmsforzohocrm__Saino_SMS_Templates",APIData:req_data,Trigger:["workflow"]}).then(function(response){
						var responseInfo	= response.data[0];
						var resCode			= responseInfo.code;
						if(resCode == 'SUCCESS'){
							document.getElementById("ErrorText").innerHTML = "Your SMS Template saved successfully.";
							setTimeout(function(){
								ZOHO.CRM.UI.Popup.closeReload();
							}, 1500);
						}
						else{
							document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						}
					});
	        	}
	        	else{
					ZOHO.CRM.API.insertRecord({Entity:"sainosmsforzohocrm__Saino_SMS_Templates",APIData:req_data}).then(function(response){
						var responseInfo	= response.data[0];
						var resCode			= responseInfo.code;
						if(resCode == 'SUCCESS'){
							document.getElementById("ErrorText").innerText = "Your SMS Template saved successfully.";
							var recordId	= responseInfo.details.id;
							setTimeout(function(){
								ZOHO.CRM.UI.Record.open({Entity:"sainosmsforzohocrm__Saino_SMS_Templates",RecordID:recordId})
								.then(function(data){
									ZOHO.CRM.UI.Popup.close();
								})
							}, 1500);								
						}
						else{
							document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
						}
					});
				}	
			}		
        }
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
			}
			else{
				document.getElementById("send").innerText="Send";
			}
		}
		function openDatePicker(){
    		document.getElementById("dateTime").style.display= "block";
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
    		document.getElementById("dateTime").style.display= "none";
    		document.getElementById("Error").style.display= "none";
    		document.getElementById("scheduleCheck").checked =true;
    		document.getElementById("send").innerText="Schedule";
    		var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time;
		}


