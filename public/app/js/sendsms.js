		var emailContent = [];
		var emailContentText = "";
		var startIndex =0;
		var endIndex = 0;
		var currentEditor="";
		var subject="";
		var sel;
		var range;
		var calendarsMap;
		var recordIds;
		var recordModule;
		var ButtonPosition;
		var smsTemplates;
		var templateId;
		var scheduledTime;
		var func_name;
		var countryCode;
		var moduleFields;
		var userFields;
		var historyFields;
		var authToken;
		var authId;
		var src;
		var currentRecords =[];
		var users =[];
		var defaultModules=["Leads","Contacts","Accounts"];
		var phoneField="Mobile";
		var recipientName;
		var isLoginUser= false;
		var currentLoginUser;
		var fetchedUsers=[];
		var users=[];
        document.addEventListener("DOMContentLoaded", function(event) {
        	var timeList=["12:00 AM","12:15 AM","12:30 AM","12:45 AM","01:00 AM","01:15 AM","01:30 AM","01:45 AM","02:00 AM","02:15 AM","02:30 AM","02:45 AM","03:00 AM","03:15 AM","03:30 AM","03:45 AM","04:00 AM","04:15 AM","04:30 AM","04:45 AM","05:00 AM","05:15 AM","05:30 AM","05:45 AM","06:00 AM","06:15 AM","06:30 AM","06:45 AM","07:00 AM","07:15 AM","07:30 AM","07:45 AM","08:00 AM","08:15 AM","08:30 AM","08:45 AM","09:00 AM","09:15 AM","09:30 AM","09:45 AM","10:00 AM","10:15 AM","10:30 AM","10:45 AM","11:00 AM","11:15 AM","11:30 AM","11:45 AM","12:00 PM","12:15 PM","12:30 PM","12:45 PM","01:00 PM","01:15 PM","01:30 PM","01:45 PM","02:00 PM","02:15 PM","02:30 PM","02:45 PM","03:00 PM","03:15 PM","03:30 PM","03:45 PM","04:00 PM","04:15 PM","04:30 PM","04:45 PM","05:00 PM","05:15 PM","05:30 PM","05:45 PM","06:00 PM","06:15 PM","06:30 PM","06:45 PM","07:00 PM","07:15 PM","07:30 PM","07:45 PM","08:00 PM","08:15 PM","08:30 PM","08:45 PM","09:00 PM","09:15 PM","09:30 PM","09:45 PM","10:00 PM","10:15 PM","10:30 PM","10:45 PM","11:00 PM","11:15 PM","11:30 PM","11:45 PM"];
    		var timeOptions ="";
    		timeList.forEach(function(time){
    			 timeOptions = timeOptions +"<option  value='"+time+"'>"+time+"</option>"
    		});
        	// var cusotmerData = ["Owner", "Email", "$currency_symbol", "Other_Phone", "Mailing_State", "$upcoming_activity", "Other_State", "Other_Country", "Last_Activity_Time", "Department", "$process_flow", "Assistant", "Mailing_Country", "id", "$approved", "Reporting_To", "$approval", "Other_City", "Created_Time", "$editable", "Home_Phone", "$status", "Created_By", "Secondary_Email", "Description", "Vendor_Name", "Mailing_Zip", "$photo_id", "Twitter", "Other_Zip", "Mailing_Street", "Salutation", "First_Name", "Full_Name", "Asst_Phone", "Record_Image", "Modified_By", "Skype_ID", "Phone", "Account_Name", "Email_Opt_Out", "Modified_Time", "Date_of_Birth", "Mailing_City", "Title", "Other_Street", "Mobile", "Last_Name", "Lead_Source", "Tag", "Fax"];
        	ZOHO.embeddedApp.on("PageLoad", function(record) {
               	recordIds = record.EntityId;
               	recordModule = record.Entity;
               	ButtonPosition = record.ButtonPosition;
               	ZOHO.CRM.UI.Resize({height:"520",width:"700"}).then(function(data){
					console.log(data);
				});
				ZOHO.CRM.META.getFields({"Entity":"Users"}).then(function(data){
					userFields = data.fields;
					data.fields.forEach(function(field){
						addListItem("dropdown-menu-user",field.field_label,"dropdown-item","Users."+field.field_label);
					});
				});	
				ZOHO.CRM.META.getFields({"Entity":"sainosmsforzohocrm__Saino_SMS_History"}).then(function(data){
					historyFields = data.fields;
				});	
				ZOHO.CRM.CONFIG.getCurrentUser().then(function(data){
					currentLoginUser = data.users[0];
				});
					
               	ZOHO.CRM.API.searchRecord({Entity:"sainosmsforzohocrm__Saino_SMS_Templates",Type:"criteria",Query:"(sainosmsforzohocrm__Module_Name:equals:"+recordModule+")",delay:"false"})
				.then(function(data){
					smsTemplates = data.data;
					var templateList="";
					if(data.data){
						for(let i=0;i <data.data.length;i++){
							templateList =templateList+ '<li class="templateItem" id="'+data.data[i].id+'" onclick="showsms(this)"></li>';
						}
						$('#templateList').append(templateList);
						if(templateList == ""){
							$('#templateList').append('<li style="text-align:center;">No Templates</li>');
						}
						else{
							for(let i=0;i <data.data.length;i++){
								document.getElementById(data.data[i].id).innerText = data.data[i].Name;
							}
						}
					}
					else{
						$('#templateList').append('<li style="text-align:center;">No Templates</li>');
					}	
					
	                ZOHO.CRM.API.getRecord({Entity:recordModule,RecordID:recordIds})
					.then(function(data){
						currentRecords = data.data;
						if(record.ButtonPosition == "DetailView"){
                			func_name = "sainofirstsmsforzohocrm__smshandler";
							document.getElementById("loader").style.display= "none";
							document.getElementById("container").style.display= "block";
							// var Mobile = data.data[0].Mobile
							// Mobile = Mobile.replace(/\D/g,'');
							// if(countryCode && Mobile.substring(0,countryCode.length) != countryCode){
							// 	Mobile = countryCode+""+Mobile;
							// }
							// document.getElementById("editNumber").value =  Mobile;
							// if(data.data[0].Mobile == null || data.data[0].Mobile == ""){
							// 	document.getElementById("ErrorText").innerText = "Mobile field is empty.";
				   //      		document.getElementById("Error").style.display= "block";
							// }
							selectModule(recordModule,data.data[0]);
						}
						else{
							document.getElementById("editNumberui").style.display= "none";
							document.getElementById("loader").style.display= "none";
							document.getElementById("container").style.display= "block";
							func_name = "sainofirstsmsforzohocrm__bulksms";
							selectModuleforBulk(recordModule);
						}	
					})
				}) 
	        });
        	ZOHO.embeddedApp.init().then(function(){
				$('#timeList').append(timeOptions);
	   //  		var aestTime = new Date().toLocaleString("en-US", {timeZone: "Australia/Brisbane"});
				// aestTime = new Date(aestTime);
	    		$('#datepicker').datepicker().datepicker('setDate',new Date());
	    		var date = document.getElementById("datepicker").value;
	    		var time = document.getElementById("timeList").value;
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	        	ZOHO.CRM.API.getOrgVariable("sainosmsforzohocrm__credentials").then(function(apiKey){
					if(apiKey && apiKey.Success && apiKey.Success.Content && !valueExists(apiKey.Success.Content)){
						document.getElementById("ErrorText").innerText = "Please enter your Saino Credentials in extension settings page.";
			        	document.getElementById("Error").style.display= "block";
					}
					else{
						sainoCreds = JSON.parse(apiKey.Success.Content);
						if(!sainoCreds.senderid || !sainoCreds.route || !sainoCreds.Token){
							document.getElementById("ErrorText").innerText = "Please enter your Saino Credentials in extension settings page.";
			        		document.getElementById("Error").style.display= "block";
						}
					}
				});
        	});
        	const el = document.getElementById('emailContentEmail');

			el.addEventListener('paste', (e) => {
			  // Get user's pasted data
			  let data = e.clipboardData.getData('text/html') ||
			      e.clipboardData.getData('text/plain');
			  
			  // Filter out everything except simple text and allowable HTML elements
			  let regex = /<(?!(\/\s*)?()[>,\s])([^>])*>/g;
			  data = data.replace(regex, '');
			  
			  // Insert the filtered content
			  document.execCommand('insertHTML', false, data);

			  // Prevent the standard paste behavior
			  e.preventDefault();
			});
			var content_id = 'emailContentEmail';  
			max = 2000;
			//binding keyup/down events on the contenteditable div
			$('#'+content_id).keyup(function(e){ check_charcount(content_id, max, e); });
			$('#'+content_id).keydown(function(e){ check_charcount(content_id, max, e); });

			function check_charcount(content_id, max, e)
			{   
			    if(e.which != 8 && $('#'+content_id).text().length > max)
			    {
			    	document.getElementById("ErrorText").innerText = "Message should be within 2000 characters.";
	        		document.getElementById("Error").style.display= "block";
	        		// document.getElementById("ErrorText").style.color="red";
					setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
			       // $('#'+content_id).text($('#'+content_id).text().substring(0, max));
			       e.preventDefault();
			    }
			}
        });

		function selectModuleforBulk(module){
			var customerData = [];
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			document.getElementById("dropdown-menu-email").innerHTML="";
			Promise.all([ZOHO.CRM.META.getFields({"Entity":module}),ZOHO.CRM.API.getOrgVariable("sainosmsforzohocrm__phoneFields")]).then(function(data){
				if(data[1] && data[1].Success && data[1].Success.Content){
					var phoneFieldsSetting =JSON.parse(data[1].Success.Content);
    				phoneField = phoneFieldsSetting.phoneField[module];
    				countryCode = phoneFieldsSetting.countryCode;
    				if(phoneFieldsSetting["isLoginUser"] == true){
    					isLoginUser = true;
    				}
				}
				moduleFields = data[0].fields;
				data[0].fields.forEach(function(field){
					addListItem("dropdown-menu-email",field.field_label,"dropdown-item",module+"."+field.field_label);
				});
			});	
		}
		function selectModule(module,record){
			document.getElementById("moduleFields").innerText = "Insert "+module+" Fields";
			var customerData = [];
			var phoneFields = [];	
			Promise.all([ZOHO.CRM.META.getFields({"Entity":module}),ZOHO.CRM.API.getOrgVariable("sainosmsforzohocrm__phoneFields")]).then(function(data){
				if(data[1] && data[1].Success && data[1].Success.Content){
					var phoneFieldsSetting =JSON.parse(data[1].Success.Content);
    				phoneField = phoneFieldsSetting[module];
    				if(phoneFieldsSetting["isLoginUser"] == true){
    					isLoginUser = true;
    				}
				}
				moduleFields = data[0].fields;
				data[0].fields.forEach(function(field){
					customerData.push(field.field_label);
				})
				document.getElementById("dropdown-menu-email").innerHTML="";
				customerData.forEach(function(field){
					addListItem("dropdown-menu-email",field,"dropdown-item",module+"."+field);
				});	
				var NumberList = "";
				var selectedNumber = "";
				var lookupModules=[];
				var errorMessage ="";
				data[0].fields.forEach(function(field){
					if(field.data_type == "phone"){
						errorMessage = errorMessage+field.field_label+"\\";
					}
					if(record[field.api_name] != null){
						if(field.data_type == "phone"){
							if(selectedNumber == "" || (phoneField && phoneField == field.api_name)){
								selectedNumber = field.field_label+'('+record[field.api_name]+')';
							}
							NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">'+field.field_label+'('+record[field.api_name]+') </li>';
						}
						else if(field.data_type == "lookup" && defaultModules.indexOf(field.lookup.module.api_name) != -1){
							lookupModules.push(field);
						}	
					}	
				});	
				if(defaultModules.indexOf(module) != -1){
					if(NumberList == ""){
						document.getElementById("ErrorText").innerText = errorMessage.slice(0, -1)+" field is empty.";
						document.getElementById("Error").style.display= "block";
					}
					else{
						setNumber({"innerText":selectedNumber});
					}
					$('#NumberList').append(NumberList);
					document.getElementById("loader").style.display= "none";
					document.getElementById("container").style.display= "block";
				}	
				else{
					var fetchList =[];
					lookupModules.forEach(function(lookupField){
						fetchList.push(ZOHO.CRM.META.getFields({"Entity":lookupField.lookup.module.api_name}),ZOHO.CRM.API.getRecord({Entity:lookupField.lookup.module.api_name,RecordID:record[lookupField.api_name].id}));
					});
					Promise.all(fetchList).then(function(dataFields){
						for(let i=0;i<lookupModules.length;i++){
							dataFields[2*i+0].fields.forEach(function(field){
								if(field.data_type == "phone" && dataFields[2*i+1].data[0][field.api_name] != null){
									if(selectedNumber == "" || (phoneField && phoneField == lookupModules[i].api_name+"."+field.api_name)){
										selectedNumber = lookupModules[i].lookup.module.api_name+' '+field.field_label+'('+dataFields[2*i+1].data[0][field.api_name]+')';
									}
									NumberList =NumberList+ '<li class="templateItem" onclick="setNumber(this)">'+lookupModules[i].lookup.module.api_name+' '+field.field_label+'('+dataFields[2*i+1].data[0][field.api_name]+') </li>';
								}	
							});	
						}
						if(NumberList == ""){
							document.getElementById("ErrorText").innerText = "Mobile field is empty.";
							document.getElementById("Error").style.display= "block";
						}
						else{
							setNumber({"innerText":selectedNumber});
						}
						$('#NumberList').append(NumberList);
						document.getElementById("loader").style.display= "none";
						document.getElementById("container").style.display= "block";
					});
				}

			});	
		}
		function setNumber(editor){
			var selectedNumber =editor.innerText.substring(editor.innerText.indexOf("(")+1,editor.innerText.indexOf(")"));
			if(selectedNumber){
    			selectedNumber = selectedNumber.replace(/\D/g,'');

				if(countryCode && countryCode != "0" && selectedNumber.length > countryCode.length && selectedNumber.substring(0,countryCode.length) != countryCode){
					selectedNumber = countryCode+""+selectedNumber;
				}
			}	
			if(document.getElementById("selectedNumber")){
				document.getElementById("selectedNumber").innerText =  editor.innerText;
				document.getElementById("editNumber").value =  selectedNumber;
				document.getElementById("numbertooltiptext").innerText = editor.innerText;
			}	
		}
        function updateOrgVariables(apiname,value,key){
        	
    		if(apiname == "readreceipt__ulgebraApiKey"){
    			document.getElementById("save").innerText = "Saving...";
    			value = document.getElementById("apikey").value;
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "Saving...";
    			document.getElementById("Error").style.display= "block";
    		}
    		ZOHO.CRM.CONNECTOR.invokeAPI("crm.set", {"apiname": apiname,"value": value}).then(function(res){
    			document.getElementById("ErrorText").innerText = "Saved";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 500);
    		});
        }
        function showsms(editor){
			for(var i=0; i<smsTemplates.length;i++){
				if(smsTemplates[i].id == editor.id){
					templateId = smsTemplates[i].id;
					document.getElementById("selectedTemplate").innerText = smsTemplates[i].Name;
					document.getElementById("tooltiptext").innerText = smsTemplates[i].Name;
					
					document.getElementById("emailContentEmail").innerText = smsTemplates[i].sainosmsforzohocrm__Message;
					break;
				}
			}
		}
		function valueExists(val) {
		    return val !== null && val !== undefined && val.length > 1 && val!=="null";
		}
        function sendSMS(){
        	var MobileNumber = document.getElementById("editNumber").value;
        	var toId="";
        	var toModule = recordModule?document.getElementById("selectedNumber").innerText.substring(0,7):"";
        	var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		// $('#emailContentEmail').html($('#emailContentEmail').html().replace(/&nbsp;/g,' '))
    		if(document.getElementById("emailContentEmail").innerText.length >1600){
    			document.getElementById("ErrorText").innerText = "Message should be within 1600 characters.";
        		document.getElementById("Error").style.display= "block";
        		document.getElementById("Error").style.color="red";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
    		}
    		else if(scheduledTime && new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    			document.getElementById("Error").style.display= "block";
    		}
        	else if(document.getElementById("emailContentEmail").innerText.replace(/\n/g,"").replace(/\t/g,"").replace(/ /g,"").trim() == ""){
        		document.getElementById("ErrorText").innerText = "Message cannot be empty.";
        		document.getElementById("Error").style.display= "block";
				setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else if(func_name == "sainofirstsmsforzohocrm__smshandler" && (MobileNumber == null || MobileNumber == "")){
        		document.getElementById("ErrorText").innerText = "Mobile field is empty.";
		        document.getElementById("Error").style.display= "block";
		        setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
        	}
        	else{
        		var notes = "";
        		if(func_name == "sainofirstsmsforzohocrm__bulksms"){
        			MobileNumber =null;
        			notes = "\n("+recordModule +" without mobile number will be ignored.)";
        		}
        		var error=[];
	        	document.getElementById("ErrorText").innerText = "Sending... "+notes;
				document.getElementById("Error").style.display= "block";
	        	var message = a = $('<div id="dummytext" contenteditable=true></div>').html($('#emailContentEmail').html().replace(/&nbsp;/g,' ')).text();
				var successRecords=0;
	        	var recordsLength = currentRecords.length;
	        	var recordIndex = 0;
				currentRecords.forEach(function(currentRecord){
	        		Promise.all([checkMobileNumber(MobileNumber,currentRecord),getOwnwer(currentRecord)]).then(function(numberresp){
	        			MobileNumber = numberresp[0].Mobile;
	        			if(func_name == "sainofirstsmsforzohocrm__bulksms"){
	        				toModule = numberresp[0].toModule;
	        				toId = numberresp[0].toId;
	        				recipientName = numberresp[0].recipientName;
	        			}	
		        		var recordId = currentRecord.id;
		        		var argumentsData = {
							"message" : message,
							"recordModule" : recordModule,
							"scheduledTime":scheduledTime,
							"templateId":templateId,
							"recordId" : recordId,
							"to":MobileNumber
						};
						var filledMessage = JSON.parse(JSON.stringify(getMessageWithFields(argumentsData,currentRecord)));
						if(!recipientName && currentRecord.Full_Name){
							recipientName = currentRecord.Full_Name;
						}
						if(recipientName){
							var name = "SMS to " + recipientName;
						}
						else{
							var name = "SMS to " + MobileNumber;
						}
						var req_data={"Name":name,"sainosmsforzohocrm__Message":filledMessage,"sainosmsforzohocrm__Recipient_Number":MobileNumber};
						historyFields.forEach(function(field){
							if(field.data_type == "lookup" && field.lookup.module.api_name == recordModule){
								req_data[field.api_name]=currentRecord.id;
							}
						});
						if(recordModule == "Deals" && toId && toModule){
							req_data["sainosmsforzohocrm__"+toModule]=toId;
						}
						filledMessage = filledMessage.trim();
						if(filledMessage.length > 2000)
						{
							document.getElementById("ErrorText").innerText = "Message is Too Large.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return ;
						}
						else if(filledMessage.length < 1)
						{
							document.getElementById("ErrorText").innerText = "Merge Fields value is empty.";
			        		document.getElementById("Error").style.display= "block";
							setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
							return;
						}
						if(argumentsData.scheduledTime != null)
						{
							time = argumentsData.scheduledTime.substring(0,19) + "+00:00";
							req_data["sainosmsforzohocrm__Scheduled_Time"]=time.toString();
							req_data["sainosmsforzohocrm__Status"]="Scheduled";
						}
						console.log(req_data);
						if(!argumentsData.scheduledTime)
						{   
							to = argumentsData.to?argumentsData.to.replace(/\D/g,''):"";
							if(to){
							url = "https://api.sainofirst.com/api/apis/bulk-sms?token=" + sainoCreds.Token + "&senderid=" + sainoCreds.senderid + "&route=" + sainoCreds.route + "&number=" + to + "&message=" + encodeURIComponent(filledMessage);
							var request = {
				                url: url,
				                headers: {
				                	"Content-Type": "application/json"
				                },
				            };
							ZOHO.CRM.HTTP.get(request).then(function(resp){
								console.log(resp);
								recordIndex = recordIndex+1;
								if(resp.toString().indexOf("Sms sent successfully") != -1)
								{
									successRecords=successRecords+1;
									req_data["sainosmsforzohocrm__Status"]="Sent";
									ZOHO.CRM.API.insertRecord({Entity:"sainosmsforzohocrm__Saino_SMS_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
										var responseInfo	= response.data[0];
										var resCode			= responseInfo.code;
										if(resCode == 'SUCCESS'){
											if(func_name != "sainofirstsmsforzohocrm__bulksms"){
												document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
												setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
											}	
										}
										else{
											if(func_name != "sainofirstsmsforzohocrm__bulksms"){
												document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
												setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
											}	
										}
									});
									if(recordIndex == recordsLength && func_name == "sainofirstsmsforzohocrm__bulksms"){
										document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
										setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
									}
									
								}
								else{
									try{
										if(JSON.parse(resp).status == false && JSON.parse(resp).message){
											resp=JSON.parse(resp).message;
										}
									}
									catch(e){
										console.log(e);
									}
									error.push(resp);
									if(recordIndex == recordsLength && func_name == "sainofirstsmsforzohocrm__bulksms"){
										if(successRecords == 0){
											console.log(error);
											if(error.length){
												document.getElementById("ErrorText").innerHTML = error[0];
											}
											else{
												document.getElementById("ErrorText").innerHTML = "All chosen "+phoneField+" fields are empty or invalid";
											}
										}
										else{
											document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
											setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
										}
									}
									if(resp.toString() == "" || resp.toString().indexOf("Request-URI Too Large") != -1)
									{
										document.getElementById("ErrorText").innerHTML = "Message is Too Large";
										setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
									}
									else if(resp.toString() != "")
									{
										if(func_name != "sainofirstsmsforzohocrm__bulksms"){
											if(error.length){
												document.getElementById("ErrorText").innerHTML = error[0];
											}
											else{
												document.getElementById("ErrorText").innerHTML = "Entered API Key/Route/Sender are invalid";
											}
											setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
										}	
									}
								}	
							});	
							}
							else{
								recordIndex = recordIndex+1;
								if(recordIndex == recordsLength && func_name == "sainofirstsmsforzohocrm__bulksms"){
									if(successRecords == 0){
										console.log(error);
										if(error.length){
											document.getElementById("ErrorText").innerHTML = error[0];
										}
										else{
											document.getElementById("ErrorText").innerHTML = "All chosen "+phoneField+" fields are empty or invalid";
										}
									}
									else{
										document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been sent successfully.</div>';
										setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload(); }, 1500);
									}
								}
							}	
						}
						else
						{
							ZOHO.CRM.API.insertRecord({Entity:"sainosmsforzohocrm__Saino_SMS_History",APIData:req_data,Trigger:["workflow"]}).then(function(response){
								recordIndex = recordIndex+1;
								var responseInfo	= response.data[0];
								var resCode			= responseInfo.code;
								if(resCode == 'SUCCESS'){
									successRecords=successRecords+1;
									if(func_name != "sainofirstsmsforzohocrm__bulksms"){
										document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
										setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
									}	
									
								}
								else{
									if(func_name != "sainofirstsmsforzohocrm__bulksms"){
										document.getElementById("ErrorText").innerText = "Opps! Something went wrong from server side. Please try after sometimes!!!";
						        		document.getElementById("Error").style.display= "block";
										setTimeout(function(){document.getElementById("Error").style.display= "none"; }, 1500);
									}	
								}
								if(recordIndex == recordsLength && func_name == "sainofirstsmsforzohocrm__bulksms"){
									document.getElementById("ErrorText").innerHTML ='<div class="material-icons" style="float:left;">check</div><div style="float:left;padding-left:5px;">Your SMS has been scheduled successfully.</div>';
									setTimeout(function(){ZOHO.CRM.UI.Popup.closeReload();}, 1500);
								}
							});
						}
					});	
				});
			}		
        }
        function getOwnwer(currentRecord){
        	if(currentRecord.Owner != null && currentRecord.Owner.id != null && fetchedUsers.indexOf(currentRecord.Owner.id) == -1)
			{
				var ownerId = currentRecord.Owner.id;
				return ZOHO.CRM.API.getUser({ID:ownerId}).then(function(data){
					fetchedUsers.push(ownerId); 
					users.push(data.users[0]);
				    return true;
				})
			}
			else{
				return true;
			}
        }
        function getMessageWithFields(messageDetails,currentRecord){
        	var message = JSON.parse(JSON.stringify(messageDetails.message));
			var customerData=[];
			var module = messageDetails.recordModule;
			moduleFields.forEach(function(field){
				var replace = "\\${"+module+"."+field.field_label+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[field.api_name] != null)
				{
					var value = currentRecord[field.api_name];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});	
			customerData.forEach(function(field){
				if(field == "Contact Id" || field == "Lead Id" || field == "Account Id" || field == "Deal Id")
				{
					var rfield = "id";
				}
				else
				{
					var rfield = field;
				}
				var replace = "\\${"+module+"."+field+"}";
				var re = new RegExp(replace,"g");
				if(currentRecord[rfield.replace(/ /g,"_")] != null)
				{
					var value = currentRecord[rfield.replace(/ /g,"_") + ""];
					if(value.name)
					{
						value = value.name;
					}
					
					message = message.replace(re,value);
				}
				else
				{
					message = message.toString().replace(re," ");
				}
			});
			if(currentRecord.Owner != null)
			{
				var ownerId = currentRecord.Owner.id;
			}
			if(ownerId != null || isLoginUser)
			{
				var currentUser;
				if(isLoginUser && currentLoginUser){
					currentUser = currentLoginUser;
				}
				else{
					users.forEach(function(user){
						if(user.id == ownerId){
							currentUser = user;
						}
					})
				}
				
				if(currentUser){
					userFields.forEach(function(field){
						var replace = "\\${Users."+field.field_label+"}";
						var re = new RegExp(replace,"g");
						if(currentUser[field.api_name] != null)
						{
							var value = currentUser[field.api_name];
							if(value.name)
							{
								value = value.name;
							}
							
							message = message.replace(re,value);
						}
						else
						{
							message = message.toString().replace(re," ");
						}
					});
					return message;
				}
			}
			else{
				return message;
			}
			
        }
        function checkMobileNumber(Mobile,currentRecord){
			if(!Mobile){
				return Promise.all([getMobileNumber(Mobile,currentRecord)]).then(function(resp){
					if(resp[0].Mobile){
						Mobile = resp[0].Mobile.replace(/\D/g,'');
						var request ={
					        url : "https://rest.messagebird.com/lookup/" + Mobile,
					        headers:{
					              Authorization:"AccessKey 7NMPor0R8DofSHH61SpViNNqQ",
					        }
					    }
					    return ZOHO.CRM.HTTP.get(request).then(function(phoneData){
					    	if(JSON.parse(phoneData).countryPrefix == null){
					    		if(countryCode && countryCode != "0" && Mobile.length > countryCode.length){
									Mobile = countryCode+""+Mobile;
								}
					    	}
					    	return {"Mobile":Mobile,"toModule":resp[0].toModule,"toId":resp[0].toId,"recipientName":resp[0].recipientName};
					    });	
					}
					else{
						return {};
					}    
				});
			}
			else{
				return {"Mobile":Mobile};
			}	
		}
		function getMobileNumber(Mobile,currentRecord){
			if(phoneField.indexOf(".") != -1){
				var toModuleField = phoneField.substring(0,phoneField.indexOf("."));
				var dealphoneField = phoneField.substring(phoneField.indexOf(".")+1);
				if(recordModule == "Deals"){
					toModuleField=toModuleField.substring(0,toModuleField.length-1)+"_Name";
				}
				if(currentRecord[toModuleField] && currentRecord[toModuleField].id){
					var toId = currentRecord[toModuleField].id ;
					var toModule;
					moduleFields.forEach(function(field){
						if(field.api_name == toModuleField){
							toModule = field.lookup.module.api_name;
						}
					});
					return ZOHO.CRM.API.getRecord({Entity:toModule,RecordID:toId}).then(function(contactData){
						if(contactData.data[0][dealphoneField] != null && contactData.data[0][dealphoneField] != ""){
							if(toModule == "Contacts" || toModule == "Leads" ){
								var recipientName = contactData.data[0].Full_Name;
							}
							else if(toModule == "Accounts"){
								var recipientName = contactData.data[0].Account_Name;
							}	
							return {"Mobile":contactData.data[0][dealphoneField],"toModule":toModule,"toId":toId,"recipientName":recipientName};
						}
						else{
							return {"Mobile":null};
						}
					});	
				}
				else{
					return {"Mobile":null};
				}
			}	
			else{
				return {"Mobile":currentRecord[phoneField]};
			}
		}
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
		}
        function addListItem(id,text,className,value){
			if(className == "dropdown-item"){
				var linode = '<li class="'+className+'"><button class="'+className+'" onclick="insert(this)">'+text+'<input type="hidden" value="'+value+'"></button></li>';
			}
			else{
				var linode = '<li class="'+className+'">'+text+'</li>';
			}
			$('#'+id).append(linode);

        }
		function styling(tag)
		{
			document.execCommand(tag);
		}
		function link(){
			$("#linkForm").slideToggle("slow");
		}
		function image(){
			$("#imageForm").slideToggle("slow");
		}
		function addLink(){
			var href = document.getElementById("linkUrl").value;
		    if (range) {
				if(range.startOffset == range.endOffset){
					if(range.commonAncestorContainer.parentNode.href){
						range.commonAncestorContainer.parentNode.href=href;
					}
					else{
						var span = document.createElement('a');
						span.setAttribute('href',href);
						span.innerText = href;
						range.insertNode(span);
			        	range.setStartAfter(span);
			        }	
				}
				else{
					var data = range.commonAncestorContainer.data;
					var start = range.startOffset;
					var end = range.endOffset;
					range.commonAncestorContainer.data="";
					var span = document.createElement('span');
					span.appendChild( document.createTextNode(data.substring(0,start)) );
					var atag = document.createElement('a');
					atag.setAttribute('href',href);
					atag.innerText = data.substring(start,end);
					span.appendChild(atag);
					span.appendChild( document.createTextNode(data.substring(end)) );
					range.insertNode(span);
		        	range.setStartAfter(span);
				}
		        range.collapse(true);
		    }
			$("#linkForm").slideToggle("slow");
		}
		function addImage(){
			var href = document.getElementById("imageUrl").value;
			var span = document.createElement('img');
			span.setAttribute('src',href);
			span.innerText = href;
			range.insertNode(span);
        	range.setStartAfter(span);
			$("#imageForm").slideToggle("slow");
		}
		function openlink(){
			sel = window.getSelection();
		    if (sel && sel.rangeCount) {
		        range = sel.getRangeAt(0);
		      }  
			if(range && range.commonAncestorContainer.wholeText){
				if(range.commonAncestorContainer.parentNode.href){
					document.getElementById("linkUrl").value = range.commonAncestorContainer.parentNode.href;
					$("#linkForm").slideToggle("slow");
				}
			}	
		}
		function insert(bookingLink){
    		// var bookingLink = this;
			var range;

			if (sel && sel.rangeCount && isDescendant(sel.focusNode)){
		        range = sel.getRangeAt(0);
		        range.collapse(true);
    		    var span = document.createElement("span");
    		    span.appendChild( document.createTextNode('${'+bookingLink.children[0].value+'}') );
        		range.insertNode(span);
	    		range.setStartAfter(span);
		        range.collapse(true);
		        sel.removeAllRanges();
		        sel.addRange(range);
		    }    
		}
		function isDescendant(child) {
			var parent = document.getElementById("emailContentEmail");
		     var node = child.parentNode;
		     while (node != null) {
		         if (node == parent || child == parent) {
		             return true;
		         }
		         node = node.parentNode;
		     }
		     return false;
		}
		function enableSchedule(element){
			if(element.checked == true){
				document.getElementById("send").innerText="Schedule";
				var date = document.getElementById("datepicker").value;
    			var time = document.getElementById("timeList").value;
    			scheduledTime = new Date(date+" "+time).toISOString();
			}
			else{
				document.getElementById("send").innerText="Send";
				scheduledTime = undefined;
			}
		}
		function openDatePicker(){
			document.getElementById("ErrorText").innerHTML ="";
    		document.getElementById("dateTime").style.display= "block";
    		if(ButtonPosition == "DetailView"){
    			document.getElementById("dateTime").style.top= "84%";
    		}
    		else{
    			document.getElementById("dateTime").style.top= "60%";
    		}
    		document.getElementById("Error").style.display= "block";
		}	
		function scheduleClose(){
			var date = document.getElementById("datepicker").value;
    		var time = document.getElementById("timeList").value;
    		if(new Date(date+" "+time).getTime() < new Date().getTime()){
    			document.getElementById("ErrorText").innerText = "Schedule time should be in future.";
    		}
    		else{
    			document.getElementById("ErrorText").innerText = "";
	    		document.getElementById("dateTime").style.display= "none";
	    		document.getElementById("Error").style.display= "none";
	    		document.getElementById("scheduleCheck").checked =true;
	    		document.getElementById("send").innerText="Schedule";
	    		document.getElementById("scheduledDateTime").innerText=new Date(date).toDateString()+" at "+time +" ("+Intl.DateTimeFormat().resolvedOptions().timeZone+")";
	    		scheduledTime = new Date(date+" "+time).toISOString();
	    	}	
		}
		function cancel(){
			document.getElementById("Error").style.display= "none";
		}

